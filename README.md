# gitlab-portainer


Pour déployer GitLab avec une base de données PostgreSQL séparée en utilisant Portainer, vous pouvez suivre les étapes suivantes. Nous allons configurer un conteneur GitLab et un conteneur PostgreSQL distincts, puis les lier ensemble.

### Étape 1: Installer Portainer

Si ce n'est pas déjà fait, commencez par installer Portainer :

```sh
docker volume create portainer_data

docker run -d -p 9000:9000 --name portainer --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v portainer_data:/data \
  portainer/portainer-ce
```

Accédez à Portainer en ouvrant un navigateur web et en naviguant vers `http://localhost:9000`.

### Étape 2: Déployer PostgreSQL via Portainer

1. **Accéder à Portainer et connecter à votre environnement Docker :**

    - Dans le menu de gauche, cliquez sur "Containers" puis sur "Add container".
    
2. **Configurer le conteneur PostgreSQL :**

    - **Name:** postgresql
    - **Image:** postgres:latest
    - **Port mapping:** 
        - Host: 5432, Container: 5432
    - **Volumes:**
        - /srv/gitlab/postgresql:/var/lib/postgresql/data
    - **Environment variables:**
        - POSTGRES_DB: gitlabhq_production
        - POSTGRES_USER: gitlab
        - POSTGRES_PASSWORD: your_password

    Cliquez sur "Deploy the container".

### Étape 3: Déployer GitLab via Portainer

1. **Accéder à Portainer et connecter à votre environnement Docker :**

    - Dans le menu de gauche, cliquez sur "Containers" puis sur "Add container".
    
2. **Configurer le conteneur GitLab :**

    - **Name:** gitlab
    - **Image:** gitlab/gitlab-ce:latest
    - **Port mapping:** 
        - Host: 80, Container: 80
        - Host: 443, Container: 443
        - Host: 22, Container: 22
    - **Volumes:**
      ```
        - /srv/gitlab/config:/etc/gitlab
        - /srv/gitlab/logs:/var/log/gitlab
        - /srv/gitlab/data:/var/opt/gitlab
      ```
    - **Environment variables:**
       ```
        - GITLAB_OMNIBUS_CONFIG: |
            external_url 'http://<votre_adresse_ip>'
            gitlab_rails['db_adapter'] = 'postgresql'
            gitlab_rails['db_encoding'] = 'unicode'
            gitlab_rails['db_collation'] = ''
            gitlab_rails['db_database'] = 'gitlabhq_production'
            gitlab_rails['db_pool'] = 10
            gitlab_rails['db_username'] = 'gitlab'
            gitlab_rails['db_password'] = 'your_password'
            gitlab_rails['db_host'] = 'postgresql'
            gitlab_rails['db_port'] = 5432
      ```
    Cliquez sur "Deploy the container".

### Étape 4: Configurer le Réseau Docker pour la Communication

1. **Créer un réseau Docker personnalisé :**

    Dans le terminal, exécutez la commande suivante pour créer un réseau Docker personnalisé :

    ```sh
    docker network create gitlab-network
    ```

2. **Connecter les conteneurs au réseau :**

    - **PostgreSQL:**

      Dans Portainer, allez à votre conteneur PostgreSQL, cliquez sur "Duplicate/Edit" et dans la section "Network", ajoutez le réseau `gitlab-network`. Ensuite, cliquez sur "Deploy the container".

    - **GitLab:**

      Faites de même pour le conteneur GitLab, ajoutez le réseau `gitlab-network` et cliquez sur "Deploy the container".

### Étape 5: Vérification

1. **Accéder à GitLab :**

    Ouvrez un navigateur web et allez à l'adresse `http://<votre_adresse_ip>`.

2. **Configurer GitLab :**

    Suivez les instructions à l'écran pour terminer la configuration de GitLab.

### Étape 6: Configurer le Système de Sauvegarde

1. **Créer le Script de Sauvegarde :**

    Créez un script nommé `gitlab_backup.sh` avec le contenu suivant et sauvegardez-le sur votre machine locale :

    ```sh
    #!/bin/sh

    # Configuration de base
    BACKUP_DIR="/srv/gitlab/backups"
    TIMESTAMP=$(date +%Y%m%d%H%M%S)
    BACKUP_FILE="${BACKUP_DIR}/gitlab_backup_${TIMESTAMP}.tar"

    # Créez le répertoire de sauvegarde s'il n'existe pas
    mkdir -p ${BACKUP_DIR}

    # Exécutez la tâche de sauvegarde GitLab
    docker exec gitlab gitlab-backup create

    # Arrêtez GitLab pour une sauvegarde cohérente
    docker stop gitlab

    # Sauvegardez les fichiers de configuration et la base de données
    tar -cvf ${BACKUP_FILE} /srv/gitlab/config /srv/gitlab/data /srv/gitlab/logs

    # Redémarrez GitLab
    docker start gitlab

    # Supprimez les sauvegardes plus anciennes (conserver uniquement les 7 dernières)
    find ${BACKUP_DIR} -type f -name "*.tar" -mtime +7 -exec rm {} \;

    echo "Sauvegarde complète : ${BACKUP_FILE}"
    ```

2. **Créer un Dockerfile pour le Script de Sauvegarde :**

    Créez un fichier `Dockerfile` avec le contenu suivant et sauvegardez-le dans le même répertoire que votre script `gitlab_backup.sh` :

    ```Dockerfile
    FROM alpine:latest

    # Installez les dépendances nécessaires
    RUN apk add --no-cache bash tar curl

    # Copiez le script de sauvegarde dans le conteneur
    COPY gitlab_backup.sh /usr/local/bin/gitlab_backup.sh

    # Rendre le script exécutable
    RUN chmod +x /usr/local/bin/gitlab_backup.sh

    # Définir le point d'entrée
    ENTRYPOINT ["/usr/local/bin/gitlab_backup.sh"]
    ```

3. **Construire et Pusher l'Image Docker :**

    Dans le terminal, accédez au répertoire où se trouvent `gitlab_backup.sh` et `Dockerfile`, puis exécutez :

    ```sh
    docker build -t gitlab-backup:latest .
    ```

4. **Déployer le Conteneur de Sauvegarde via Portainer :**

    - Accédez à Portainer.
    - Cliquez sur "Stacks" puis sur "Add stack".
    - Nommez la stack `gitlab-backup`.
    - Dans le champ "Web editor", entrez le contenu suivant pour créer un service de sauvegarde programmé (par exemple, une fois par jour à minuit) :

    ```yaml
    version: '3.7'

    services:
      gitlab-backup:
        image: gitlab-backup:latest
        volumes:
          - /srv/gitlab:/srv/gitlab
        environment:
          - TZ=Europe/Paris
        deploy:
          restart_policy:
            condition: on-failure
          replicas: 1
          placement:
            constraints:
              - node.role == manager
          update_config:
            parallelism: 1
            delay: 10s
          rollback_config:
            parallelism: 1
            delay: 10s
          labels:
            - "cron.schedule=0 0 * * *"
    ```

    Notez que vous devez avoir le plugin Portainer Agent installé pour utiliser des stacks avec des services programmés.

### Conclusion

En suivant ces étapes, vous déployez GitLab avec une base de données PostgreSQL séparée et un système de sauvegarde automatisé en utilisant Portainer. Le conteneur GitLab sera géré et supervisé via l'interface de Portainer, et le script de sauvegarde sera exécuté automatiquement à l'aide d'un conteneur de sauvegarde programmé.