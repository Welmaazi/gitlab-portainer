#!/bin/bash

# Définir le répertoire des sauvegardes
BACKUP_DIR="/srv/backups"
CONFIG_BACKUP_DIR="$BACKUP_DIR/config"
DEST_DIR="/backup" # Changez ceci en votre répertoire de destination
RETENTION_DAYS=7

# Créer une sauvegarde GitLab
docker exec gitlab-ee gitlab-backup create STRATEGY=copy

# Sauvegarder la configuration de GitLab
mkdir -p "$CONFIG_BACKUP_DIR"
docker cp gitlab-ee:/etc/gitlab "$CONFIG_BACKUP_DIR/gitlab-config-$(date +\%Y\%m\%d\%H\%M\%S).tar.gz"

# Déplacer les sauvegardes vers le répertoire de destination
mkdir -p "$DEST_DIR"
mv "$BACKUP_DIR"/*.tar "$DEST_DIR/"
mv "$CONFIG_BACKUP_DIR"/*.tar.gz "$DEST_DIR/"

# Supprimer les sauvegardes plus anciennes que le délai configuré
find "$DEST_DIR" -type f -name '*.tar' -mtime +$RETENTION_DAYS -exec rm {} \;
find "$DEST_DIR" -type f -name '*.tar.gz' -mtime +$RETENTION_DAYS -exec rm {} \;