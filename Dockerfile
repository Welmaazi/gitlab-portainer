FROM alpine:latest

# Installez les dépendances nécessaires
RUN apk add --no-cache bash tar curl

# Copiez le script de sauvegarde dans le conteneur
COPY gitlab_backup.sh /usr/local/bin/gitlab_backup.sh

# Rendre le script exécutable
RUN chmod +x /usr/local/bin/gitlab_backup.sh

# Définir le point d'entrée
ENTRYPOINT ["/usr/local/bin/gitlab_backup.sh"]